obj-m+=shutdown_gpio.o

KPATH=$(shell readlink -f ../linux)

all:
	make -C $(KPATH) M=$(shell pwd) modules

clean:
	make -C $(KPATH) M=$(shell pwd) clean

modules_install: all
	$(MAKE) -C $(KPATH) M=$(shell pwd) modules_install
	depmod
