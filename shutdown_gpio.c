#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/gpio.h>
#include <linux/pm.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Xian Stannard");
MODULE_DESCRIPTION("Use GPIO to do final switch off of a PSU");
MODULE_VERSION("1.0");

/*
 * The GPIO number to signal on
 */
static int gpio_number = 0;
module_param(gpio_number, int, 0);
MODULE_PARM_DESC(gpio_number, "GPIO number");

/*
 * Set to 1 to drive the GPIO high (instead of low) when shutdown is
 * complete.
 */
static int inverted;
module_param(inverted, int, 0);
MODULE_PARM_DESC(gpio_number, "invert signal");

static void do_power_off(void) {
    gpio_set_value(gpio_number, inverted);
}

static int __init shutdown_gpio_init(void)
{
    int result = 0;

    // ensure inverted is either 0 or 1
    if (inverted != 0)
        inverted = 1;

    if (!gpio_is_valid(gpio_number)){
       printk(KERN_INFO "shutdown_gpio: invalid GPIO: %d\n", gpio_number);
       return -ENODEV;
    }

    result = gpio_request(gpio_number, "sysfs");
    if (result) {
        printk(KERN_ERR "shutdown_gpio: cound not request gpio");
        goto exit;
    }

    // drive the GPIO high until it is time to switch off
    result = gpio_direction_output(gpio_number, !inverted);
    if (result) {
        printk(KERN_ERR "shutdown_gpio: could not set gpio direction");
        goto free;
    }

    pm_power_off = do_power_off;
    printk(KERN_INFO "shutdown_gpio: gpio %d will be set to %d on power off", gpio_number, inverted);

    return 0;

free:
    gpio_free(gpio_number);

exit:
    return result;
}

static void __exit shutdown_gpio_exit(void)
{
    if (pm_power_off == do_power_off)
        pm_power_off = NULL;
    else
        printk(KERN_WARNING "something blatted our shutdown callback");

    /*
     * Return the GPIO to input. There is no cleanup that can be doen if
     * these calls fail the re result is ignored.
     */
    gpio_direction_input(gpio_number);
    gpio_free(gpio_number);
}

module_init(shutdown_gpio_init);
module_exit(shutdown_gpio_exit);
