# Shutdown GPIO

This is a kernel module that drives a GPIO low (or high) when the machine shut's down. It is the very last thing the machine does so that it can be used to turn of the power to the machine. The GPIO does not go low when the machine reboots.